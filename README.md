# pyenv-default-requirements

This pyenv plugin hooks into the `pyenv install` and `pyenv virtualenv`
commands to automatically install packages every time you install a new
version of Python or create a new virtualenv.

Forked from the initial [pyenv-default-packages][1] by [jawshooah][2].

## Installation

### Installing as a pyenv plugin

Make sure you have the latest pyenv version, then run:

```bash
git clone https://gitlab.com/perobertson/pyenv-default-requirements.git $(pyenv root)/plugins/pyenv-default-requirements
```

## Usage

Default packages will be installed every time you successfully install a new
version of Python with `pyenv install`, or create a virtualenv with
`pyenv virtualenv`.

The list of default packages to install are configured in
`${HOME}/.config/pyenv-default-requirements/requirements.txt`.
The packages should be specified by name, one per line.
This file should follow the pip [requirements file format][3].
For example:

```
pytest >= 2.6.4
futures; python_version < '2.7'
http://my.package.repo/SomePackage-1.0.4.zip; python_version >= '3.4'
```

A line that begins with `#` is treated as a comment and ignored. Whitespace
followed by a `#` causes the `#` and the remainder of the line to be treated
as a comment.

There is also support for multiple requirements files.
Files placed in `${HOME}/.config/pyenv-default-requirements/requirements.d/`
will be used as requirements in filesystem order.
A suggested use case for this is having the files:
- 00_pip.txt
- 01_wheel.txt
- 02_requirements.txt

The files will be installed in the order `requirements.txt` and then `requirements.d/*`.

## License

[The MIT License][4]

[1]: https://github.com/jawshooah/pyenv-default-packages
[2]: https://github.com/jawshooah
[3]: https://pip.pypa.io/en/latest/reference/pip_install.html#requirements-file-format
[4]: https://gitlab.com/perobertson/pyenv-default-requirements/-/blob/master/LICENSE
