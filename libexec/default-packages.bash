install_default_packages() {
  # Only install default packages after successfully installing Python.
  [ "$STATUS" = "0" ] || return 0

  local installed_version requirements_file args

  installed_version=$1
  requirements_dir="${XDG_CONFIG_HOME:-${HOME}/.config}/pyenv-default-requirements/requirements.d/"
  requirements_file="${XDG_CONFIG_HOME:-${HOME}/.config}/pyenv-default-requirements/requirements.txt"

  if [ -f "$requirements_file" ]; then
    args=( -r "$requirements_file" )

    # Invoke `pip install` in the just-installed Python.
    PYENV_VERSION="$installed_version" pyenv-exec python -m pip install "${args[@]}" || {
      echo "pyenv: error installing packages from  \`$requirements_file'"
    } >&2
  fi

  if [ -d "${requirements_dir}" ]; then
    requirements_files="${requirements_dir}/*"
    for requirements_file in $requirements_files; do
      args=( -r "$requirements_file" )

      # Invoke `pip install` in the just-installed Python.
      PYENV_VERSION="$installed_version" pyenv-exec python -m pip install "${args[@]}" || {
        echo "pyenv: error installing packages from  \`$requirements_file'"
      } >&2
    done
  fi
}
